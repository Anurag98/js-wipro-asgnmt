<!DOCTYPE html>
<html>
  <head>
	<meta charset=utf-8 />
	<title>'CD' details</title>
  </head>

  <body>  
	<h3>'CD' details</h3>
	<p>a. CD name</p>
	<p>b. CD publisher</p>
	<p>c. CD price</p>
	
	<br>
	<div>
	  Enter CD name:<br>
	  <input id="cdN" type="text" name="cdName" value="">
	  <br>
	  Enter CD publisher:<br>
	  <input id = "cdP" type="text" name="cdPublisher" value="">
	  <br>
	  Enter CD price:<br>
	  <input id = "cdPr" type="text" name="cdPrice" value="">
	  <br><br>
	  <button type="button" onclick = "insertVal()">Display all details</button>
	</div>
	
	<div id="display" style="display:none">
		<div style="background-color: beige">
		  <p id = "cdName">CD Name: </p>
		  <p id = "cdPub">CD Publisher: </p>
		  <p id = "cdPrice">CD Final Price: </p>
		</div>
	</div>
	
  <script>
  var CD = {};
  function insertVal()
  {
	
	CD['Name'] = document.getElementById("cdN").value;
	CD['Publisher'] = document.getElementById("cdP").value;
	CD['Price'] = document.getElementById("cdPr").value;
	if(isNaN(CD.Price))
	{
		alert("Please provide valid price..!");
		document.getElementById("display").style.display = "none";
		return;
	}
	displayAllDetails();
  }
  function displayAllDetails()
  {
	document.getElementById("cdName").innerHTML += CD.Name;
	document.getElementById("cdPub").innerHTML += CD.Publisher;
	
	var price = parseInt(CD.Price);
	var fprice = price*1.10;
	fprice = fprice*0.97;
	
	document.getElementById("cdPrice").innerHTML += fprice;
	document.getElementById("display").style.display = "inline-block";
  }
  
  </script>
  </body>
</html>